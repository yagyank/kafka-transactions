import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.*;


class ApplicationTest {

  private KafkaProducerClient transactionalProducer;
  private KafkaNonTransactionalProducerClient nonTransactionalProducer;
  private TransactionalConsumer transactionalConsumer;
  private NonTransactionalConsumer nonTransactionalConsumer;

  @BeforeEach
  public void setUp() {
    transactionalProducer = new KafkaProducerClient();
    nonTransactionalProducer = new KafkaNonTransactionalProducerClient();
    transactionalConsumer = new TransactionalConsumer();
    nonTransactionalConsumer = new NonTransactionalConsumer();
  }

  @Test
  void shouldProduceAndConsumeTransactionallyToKafka() throws InterruptedException, TimeoutException, ExecutionException {
    var executorService = Executors.newCachedThreadPool();
    var future = executorService.submit(() -> readTransactionallyFromKafka(5));
    produceTransactionallyToKafka(List.of(1, 1, 3, 4, 5), "test");
    var result = future.get(4, TimeUnit.SECONDS);

    final List<String> expected = List.of("1", "1", "3", "4", "5");
    assertTrue(expected.containsAll(result) &&
        result.containsAll(expected) && (expected.size() == result.size()));
  }

  @Test
  void shouldNotConsumeAnyMessageFromAbortedTransaction() throws InterruptedException, TimeoutException, ExecutionException {

    var executorService = Executors.newCachedThreadPool();
    var future = executorService.submit(() -> readTransactionallyFromKafka(5));
    try {
      produceTransactionallyToKafkaWithException(List.of(6, 7, 8, 9, 10));
    } catch (Exception e) {

    }
    assertThrows(TimeoutException.class, () -> future.get(4, TimeUnit.SECONDS));

  }

  @Test
  void shouldConsumeAnyMessageFromAbortedTransactionForNonTransactionalConsumer() throws InterruptedException, TimeoutException, ExecutionException {

    var executorService = Executors.newCachedThreadPool();
    var messagesToReceive = 2;
    var future = executorService.submit(() -> readNonTransactionallyFromKafka(messagesToReceive));
    try {
      produceTransactionallyToKafkaWithException(List.of(11, 12));
    } catch (Exception e) {

    }
    var result = future.get(4, TimeUnit.SECONDS);
    assertEquals(List.of("11", "12"), result);
  }

  @Test
  void shouldConsumeAnyMessageFromTransactionalConsumerNonTransactionalProducer() throws InterruptedException, TimeoutException, ExecutionException {

    var executorService = Executors.newCachedThreadPool();
    var messagesToReceive = 2;
    var future = executorService.submit(() -> readTransactionallyFromKafka(messagesToReceive));
    try {
      produceNonTransactionallyToKafka(List.of(111, 121));
    } catch (Exception e) {

    }
    var result = future.get(4, TimeUnit.SECONDS);
    assertEquals(List.of("111", "121"), result);
  }

  private void produceTransactionallyToKafka(List<Integer> list, String topic) {
    transactionalProducer.getProducer().beginTransaction();
    list.forEach(val -> {
      var record = new ProducerRecord<>(topic, val.toString(), val.toString());
      try {
        transactionalProducer.getProducer().send(record).get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    });
    transactionalProducer.getProducer().commitTransaction();
  }

  private void produceNonTransactionallyToKafka(List<Integer> list) {
    list.forEach(val -> {
      var record = new ProducerRecord<>("test", val.toString(), val.toString());
      try {
        nonTransactionalProducer.getProducer().send(record).get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    });
  }

  private void produceTransactionallyToKafkaWithException(List<Integer> list) {
    transactionalProducer.getProducer().beginTransaction();
    list.forEach(val -> {
      var record = new ProducerRecord<>("test", val.toString(), val.toString());
      try {
        transactionalProducer.getProducer().send(record).get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    });
    throw new RuntimeException("Exception occurred");
  }

  private List<String> readTransactionallyFromKafka(int sz) {
    var result = new ArrayList<String>();
    while (result.size() < sz) {
      ConsumerRecords<String, String> records = transactionalConsumer.getKafkaConsumer().poll(Duration.ofSeconds(1));
      records.forEach(record -> result.add(record.value()));
      if (!records.isEmpty()) {
        transactionalConsumer.getKafkaConsumer().commitSync();
      }
    }
    return result;
  }

  private List<String> readNonTransactionallyFromKafka(int sz) {
    var result = new ArrayList<String>();
    while (result.size() < sz) {
      ConsumerRecords<String, String> records = nonTransactionalConsumer.getKafkaConsumer().poll(Duration.ofSeconds(1));
      records.forEach(record -> result.add(record.value()));
      if (!records.isEmpty()) {
        nonTransactionalConsumer.getKafkaConsumer().commitSync();
      }
    }
    return result;
  }
}