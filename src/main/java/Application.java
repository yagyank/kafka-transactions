import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static java.time.Duration.ofSeconds;

public class Application {

  public static void main(String[] args) {

    var kafkaClient = new KafkaProducerClient();

    var list = List.of(1);

    kafkaClient.getProducer().beginTransaction();
    list.forEach(val -> {
      var record = new ProducerRecord<>("test", "abc", "abc");
      try {
        kafkaClient.getProducer().send(record).get();
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    });
    kafkaClient.getProducer().abortTransaction();
//    kafkaClient.getProducer().commitTransaction();

    var consumer = new TransactionalConsumer();

    while (true) {
      ConsumerRecords<String, String> records = consumer.getKafkaConsumer().poll(Duration.ofSeconds(1));
      records.forEach(record -> {
        System.out.println(record.value());
      });
      if(!records.isEmpty()){
        consumer.getKafkaConsumer().commitSync();
        break;
      }
    }

//        records.records(new TopicPartition("input", 0))
//            .stream()
//            .forEach(record -> {
//              System.out.println(record.value());
//              record.value();
//            });

//    Map<TopicPartition, OffsetAndMetadata> offsetsToCommit = new HashMap<>();
//    for (TopicPartition partition : records.partitions()) {
//      List<ConsumerRecord<String, String>> partitionedRecords = records.records(partition);
//      long offset = partitionedRecords.get(partitionedRecords.size() - 1).offset();
//      offsetsToCommit.put(partition, new OffsetAndMetadata(offset + 1));
//    }

  }
}
