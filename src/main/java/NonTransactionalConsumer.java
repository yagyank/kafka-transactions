import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.List;
import java.util.Properties;

public class NonTransactionalConsumer {

  public KafkaConsumer getKafkaConsumer() {
    return kafkaConsumer;
  }

  public void setKafkaConsumer(KafkaConsumer kafkaConsumer) {
    this.kafkaConsumer = kafkaConsumer;
  }

  private KafkaConsumer kafkaConsumer;

  public NonTransactionalConsumer() {
    createConsumer();
  }

  public void createConsumer() {
    Properties consumerProps = new Properties();
    consumerProps.put("bootstrap.servers", "127.0.0.1:9092");
    consumerProps.put("group.id", "my-group-id-non-transactional");
    consumerProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    consumerProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
    consumerProps.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, "1000");
    this.kafkaConsumer = new KafkaConsumer<>(consumerProps);
    this.kafkaConsumer.subscribe(List.of("test"));
  }
}
