import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class KafkaProducerClient {

  private KafkaProducer producer;

  public KafkaProducerClient() {
    createProducer();
  }

  private void createProducer() {
    Properties props = new Properties();
    props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
    props.put(ProducerConfig.CLIENT_ID_CONFIG, "test-client");
    props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
    props.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
    props.put(ProducerConfig.TRANSACTIONAL_ID_CONFIG , "prod-1");
    this.producer = new KafkaProducer<>(props);
    this.producer.initTransactions();
  }

  public KafkaProducer getProducer() {
    return producer;
  }

  public void setProducer(KafkaProducer producer) {
    this.producer = producer;
  }
}
